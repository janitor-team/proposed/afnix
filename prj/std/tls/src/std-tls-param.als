# ----------------------------------------------------------------------------
# - std-tls-param                                                            -
# - afnix:std:tls parameters definition unit                                 -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2021 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - project section                                                          -
# ----------------------------------------------------------------------------

# system revision
const AFNIX:STD:TLS:MAJOR        3
const AFNIX:STD:TLS:MINOR        1
const AFNIX:STD:TLS:PATCH        0

# the application name
const AFNIX:STD:TLS:ANAME        "tls"
# the application title
const AFNIX:STD:TLS:TITLE        "afnix tls command layer"
# the copyright holder
const AFNIX:STD:TLS:CLEFT        "copyleft 1999-2021 by Amaury Darsch"

# ----------------------------------------------------------------------------
# - default section                                                          -
# ----------------------------------------------------------------------------

# the server default host/port
const AFNIX:STD:TLS:SERVER-HOST  "localhost"
const AFNIX:STD:TLS:SERVER-PORT  4433
