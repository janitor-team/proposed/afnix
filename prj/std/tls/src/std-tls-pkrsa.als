# ----------------------------------------------------------------------------
# - std-tls-pkrsa                                                            -
# - afnix:std:tls rsa key class module                                       -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2021 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - global section                                                           -
# ----------------------------------------------------------------------------

# the rsa key class
const afnix:std:tls:pkrsa (class)
# the rsa key nameset
try (const AFNIX:STD:TLS:PKRSA (nameset AFNIX:STD:TLS))

# ----------------------------------------------------------------------------
# - private section                                                          -
# ----------------------------------------------------------------------------

# the option messages
const AFNIX:STD:TLS:PKRSA:U-CLS-MSG "axi [i afnix-std-tls cmd-tls-pkrsa] [-]"
const AFNIX:STD:TLS:PKRSA:H-LCO-MSG "    [h]      print this help message"
const AFNIX:STD:TLS:PKRSA:V-LCO-MSG "    [v]      print system version"
const AFNIX:STD:TLS:PKRSA:V-UCO-MSG "    [V]      enable verbose mode"
const AFNIX:STD:TLS:PKRSA:D-LCO-MSG "    [d]      use decimal format"
const AFNIX:STD:TLS:PKRSA:S-LCO-MSG "    [s size] create a key by size"
const AFNIX:STD:TLS:PKRSA:I-LCO-MSG "    [i name] set the input name"
const AFNIX:STD:TLS:PKRSA:O-LCO-MSG "    [o name] set the output name"
const AFNIX:STD:TLS:PKRSA:P-UCO-MSG "    [P]      publicize the key"

# ----------------------------------------------------------------------------
# - initial section                                                          -
# ----------------------------------------------------------------------------

# preset the rsa key class
# @param argv the argument vector
trans afnix:std:tls:pkrsa:preset (argv) {
  # preini the class
  this:preini argv
  # postdo the class
  this:postdo
}

# preini the tls class
# @param argv the argument vector
trans afnix:std:tls:pkrsa:preini (argv) {
  # create an option class and bind it
  const this:opts (afnix:sys:Options AFNIX:STD:TLS:PKRSA:U-CLS-MSG)
  # register the options
  this:opts:add-unique-option 'P' AFNIX:STD:TLS:PKRSA:P-UCO-MSG
  this:opts:add-string-option 'o' AFNIX:STD:TLS:PKRSA:O-LCO-MSG
  this:opts:add-string-option 'i' AFNIX:STD:TLS:PKRSA:I-LCO-MSG
  this:opts:add-string-option 's' AFNIX:STD:TLS:PKRSA:S-LCO-MSG
  this:opts:add-unique-option 'd' AFNIX:STD:TLS:PKRSA:D-LCO-MSG
  this:opts:add-unique-option 'V' AFNIX:STD:TLS:PKRSA:V-UCO-MSG
  this:opts:add-unique-option 'v' AFNIX:STD:TLS:PKRSA:V-LCO-MSG
  this:opts:add-unique-option 'h' AFNIX:STD:TLS:PKRSA:H-LCO-MSG
  # parse the options
  try (this:opts:parse argv) {
    this:opts:usage (interp:get-error-stream)
    afnix:sys:exit 1
  }
  # check for the help option
  if (this:opts:get-unique-option 'h') {
    this:opts:usage (interp:get-output-stream)
    afnix:sys:exit 0
  }
  # check for the version option
  if (this:opts:get-unique-option 'v') {
    println (afnix:std:tls:get-copyright-message)
    println (afnix:std:tls:get-revision-message)
    afnix:sys:exit 0
  }
}

# postdo the res key class
trans afnix:std:tls:pkrsa:postdo nil {
  # check for the key size
  if (this:opts:get-unique-option 's') {
    const this:size (Integer (this:opts:get-string-option 's'))
  } (const this:size 0)
  # check for an input name
  if (this:opts:get-unique-option 'i') {
    const this:inam (this:opts:get-string-option 'i')
  } (const this:inam nil)
  # check for an output name
  if (this:opts:get-unique-option 'o') {
    const this:onam (this:opts:get-string-option 'o')
  } (const this:onam nil)
  # check for flags
  const this:vopt (this:opts:get-unique-option 'V')
  const this:dopt (this:opts:get-unique-option 'd')
  const this:popt (this:opts:get-unique-option 'P')
  # preset the key
  trans this:rkey nil
}

# execute the command
trans afnix:std:tls:pkrsa:run nil {
  # check for incompatible options
  if (and (> this:size 0) (string-p this:inam)) {
    errorln "[pkrsa] incompatible size and input name specified"
    afnix:sys:exit 1
  }
  # create a key by pkcs
  if (string-p this:inam) {
    # create a pkcs object
    const pkcs (afnix:tls:Pkrsa this:inam)
    # extract the key
    trans this:rkey (pkcs:get-key)
  }
  # create a key by size
  if (> this:size 0) {
    # generate the key
    trans this:rkey (afnix:sec:Key afnix:sec:Key:KRSA size)
  }
  if (nil-p this:rkey) (return)
  # check for publicize
  if (and this:popt (this:rkey:valid-p)) {
    trans this:dkey (this:rkey:to-public)
  }
  # check for export
  if (object-p this:onam) {
    # create a pkcs object
    const pkcs (afnix:tls:Pkrsa this:rkey)
    # write to a file
    pkcs:write this:onam
  }
  # check for verbose mode
  if this:vopt {
    # report modulus
    const m (this:rkey:get-relatif-key afnix:sec:Key:RSA-MODULUS)
    if this:dopt (println m) (println "[M] " (m:to-hexa-string))
    # report public exponent
    const e (this:rkey:get-relatif-key afnix:sec:Key:RSA-PUBLIC-EXPONENT)
    if this:dopt (println e) (println "[E] " (e:to-hexa-string))
    # report secret exponent
    const s (this:rkey:get-relatif-key afnix:sec:Key:RSA-SECRET-EXPONENT)
    if this:dopt (println s) (println "[S] " (s:to-hexa-string))
  }
}
