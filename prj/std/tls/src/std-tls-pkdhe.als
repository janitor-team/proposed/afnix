# ----------------------------------------------------------------------------
# - std-tls-pkdhe                                                            -
# - afnix:std:tls dhe key class module                                       -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2021 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - global section                                                           -
# ----------------------------------------------------------------------------

# the dhe key class
const afnix:std:tls:pkdhe (class)
# the dhe key nameset
try (const AFNIX:STD:TLS:PKDHE (nameset AFNIX:STD:TLS))

# ----------------------------------------------------------------------------
# - private section                                                          -
# ----------------------------------------------------------------------------

# the option messages
const AFNIX:STD:TLS:PKDHE:U-CLS-MSG "axi [i afnix-std-tls cmd-tls-pkdhe] [-]"
const AFNIX:STD:TLS:PKDHE:H-LCO-MSG "    [h]      print this help message"
const AFNIX:STD:TLS:PKDHE:V-LCO-MSG "    [v]      print system version"
const AFNIX:STD:TLS:PKDHE:V-UCO-MSG "    [V]      enable verbose mode"
const AFNIX:STD:TLS:PKDHE:D-LCO-MSG "    [d]      use decimal format"
const AFNIX:STD:TLS:PKDHE:S-LCO-MSG "    [s size] create a key by size"
const AFNIX:STD:TLS:PKDHE:I-LCO-MSG "    [i name] set the input name"
const AFNIX:STD:TLS:PKDHE:O-LCO-MSG "    [o name] set the output name"
const AFNIX:STD:TLS:PKDHE:C-UCO-MSG "    [C]      configure only the key"
const AFNIX:STD:TLS:PKDHE:P-UCO-MSG "    [P]      publicize the key"

# ----------------------------------------------------------------------------
# - initial section                                                          -
# ----------------------------------------------------------------------------

# preset the dhe key class
# @param argv the argument vector
trans afnix:std:tls:pkdhe:preset (argv) {
  # preini the class
  this:preini argv
  # postdo the class
  this:postdo
}

# preini the dhe class
# @param argv the argument vector
trans afnix:std:tls:pkdhe:preini (argv) {
  # create an option class and bind it
  const this:opts (afnix:sys:Options AFNIX:STD:TLS:PKDHE:U-CLS-MSG)
  # register the options
  this:opts:add-unique-option 'P' AFNIX:STD:TLS:PKDHE:P-UCO-MSG
  this:opts:add-unique-option 'C' AFNIX:STD:TLS:PKDHE:C-UCO-MSG
  this:opts:add-string-option 'o' AFNIX:STD:TLS:PKDHE:O-LCO-MSG
  this:opts:add-string-option 'i' AFNIX:STD:TLS:PKDHE:I-LCO-MSG
  this:opts:add-string-option 's' AFNIX:STD:TLS:PKDHE:S-LCO-MSG
  this:opts:add-unique-option 'd' AFNIX:STD:TLS:PKDHE:D-LCO-MSG
  this:opts:add-unique-option 'V' AFNIX:STD:TLS:PKDHE:V-UCO-MSG
  this:opts:add-unique-option 'v' AFNIX:STD:TLS:PKDHE:V-LCO-MSG
  this:opts:add-unique-option 'h' AFNIX:STD:TLS:PKDHE:H-LCO-MSG
  # parse the options
  try (this:opts:parse argv) {
    this:opts:usage (interp:get-error-stream)
    afnix:sys:exit 1
  }
  # check for the help option
  if (this:opts:get-unique-option 'h') {
    this:opts:usage (interp:get-output-stream)
    afnix:sys:exit 0
  }
  # check for the version option
  if (this:opts:get-unique-option 'v') {
    println (afnix:std:tls:get-copyright-message)
    println (afnix:std:tls:get-revision-message)
    afnix:sys:exit 0
  }
}

# postdo the dhe class
trans afnix:std:tls:pkdhe:postdo nil {
  # check for the key size
  if (this:opts:get-unique-option 's') {
    const this:size (Integer (this:opts:get-string-option 's'))
  } (const this:size 0)
  # check for an input name
  if (this:opts:get-unique-option 'i') {
    const this:inam (this:opts:get-string-option 'i')
  } (const this:inam nil)
  # check for an output name
  if (this:opts:get-unique-option 'o') {
    const this:onam (this:opts:get-string-option 'o')
  } (const this:onam nil)
  # check for flags
  const this:vopt (this:opts:get-unique-option 'V')
  const this:dopt (this:opts:get-unique-option 'd')
  const this:copt (this:opts:get-unique-option 'C')
  const this:popt (this:opts:get-unique-option 'P')
  # preset the key
  trans this:dkey nil
}

# execute the command
trans afnix:std:tls:pkdhe:run nil {
  # check for incompatible options
  if (and (> this:size 0) (string-p this:inam)) {
    errorln "[pkdhe] incompatible size and input name specified"
    afnix:sys:exit 1
  }
  # create a key by pkcs
  if (string-p this:inam) {
    # create a pkcs object
    trans pkcs (afnix:tls:Pkdhe this:inam)
    # extract the key
    trans this:dkey (pkcs:get-key)
  }
  # create a key by size
  if (> this:size 0) {
    # generate the key
    trans this:dkey (afnix:sec:Key)
    # configure the key
    this:dkey:configure afnix:sec:Key:KDHE size
  }
  if (nil-p this:dkey) (return)
  # check for renew
  if (and (not this:copt) (not (this:dkey:valid-p))) {
    if (not (this:dkey:renew)) {
      errorln "[pkdhe] cannot renew key"
      afnix:sys:exit 1
    }
  }
  # check for publicize
  if (and this:popt (this:dkey:valid-p)) {
    trans this:dkey (this:dkey:to-public)
  }
  # check for export
  if (object-p this:onam) {
    # create a pkcs object
    trans pkcs (afnix:tls:Pkdhe this:dkey)
    # write to a file
    pkcs:write this:onam
  }
  # check for verbose mode
  if this:vopt {
    # report prime p
    const p (this:dkey:get-relatif-key afnix:sec:Key:DH-P-PRIME)
    if this:dopt (println p) (println "[P] " (p:to-hexa-string))
    # report generator
    const g (this:dkey:get-relatif-key afnix:sec:Key:DH-GROUP-GENERATOR)
    if this:dopt (println g) (println "[G] " (g:to-hexa-string))
    # report group order
    const o (this:dkey:get-relatif-key afnix:sec:Key:DH-GROUP-ORDER)
    if this:dopt (println q) (println "[O] " (o:to-hexa-string))
    # report public key
    const k (this:dkey:get-relatif-key afnix:sec:Key:DH-PUBLIC-KEY)
    if this:dopt (println k) (println "[K] " (k:to-hexa-string))
    # report secret key
    const s (this:dkey:get-relatif-key afnix:sec:Key:DH-SECRET-KEY)
    if this:dopt (println s) (println "[S] " (s:to-hexa-string))
  }
}
