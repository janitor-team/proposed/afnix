# ----------------------------------------------------------------------------
# - std-sps-param                                                            -
# - afnix:std:sps parameters definition unit                                 -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2021 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - project section                                                          -
# ----------------------------------------------------------------------------

# system revision
const AFNIX:SPS:MAJOR        3
const AFNIX:SPS:MINOR        1
const AFNIX:SPS:PATCH        0

# the application name
const AFNIX:SPS:ANAME        "sps"
# the application title
const AFNIX:SPS:TITLE        "afnix sps command layer"
# the copyright holder
const AFNIX:SPS:CLEFT        "copyleft 1999-2021 by Amaury Darsch"
