// ---------------------------------------------------------------------------
// - Pkcs.cpp                                                                -
// - afnix:tls service - public key cryptographic standard implementation    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2020 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Xoid.hpp"
#include "Pkcs.hpp"
#include "AsnOid.hpp"
#include "Vector.hpp"
#include "AsnNull.hpp"
#include "AsnBits.hpp"
#include "Runnable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "AsnBuffer.hpp"
#include "AsnOctets.hpp"
#include "AsnInteger.hpp"
#include "AsnSequence.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // get the pkcs relatif number by index
  static Relatif pkcs_torval (const AsnSequence* pseq, const long index) {
    // check for nil
    if (pseq == nullptr) {
      throw Exception ("pkcs-error", "invalid nil pkcs sequence node");
    }
    // get the node by index
    auto inod = dynamic_cast <AsnInteger*> (pseq->getnode (index));
    if (inod == nullptr) {
      throw Exception ("pkcs-error", "cannot map integer node");
    }
    return inod->torelatif ();
  }


  // get a pkcs rsa private key
  static Key* pkcs_torsa (const AsnOctets* octs) {
    // check for a valid sequence
    if (octs == nullptr) return nullptr;
    // create an asn buffer by octet string
    AsnBuffer abuf (octs->tobuffer());
    // map the buffer as a node
    AsnNode* node = abuf.mapnode();
    // check for a sequence
    auto pseq = dynamic_cast <AsnSequence*> (node);
    // check the sequence length
    if (pseq->getnlen () != 9) {
      throw Exception ("pkcs-error", "invalid pkcs rsa sequence length");
    }
    // get the object version
    long vers = pkcs_torval(pseq, 0).tolong ();
    if (vers != 0L) {
      throw Exception ("pkcs-error", "invalid pkcs rsa key version");
    }
    // get the key element
    Relatif pmod = pkcs_torval (pseq, 1);
    Relatif pexp = pkcs_torval (pseq, 2);
    Relatif sexp = pkcs_torval (pseq, 3);
    Relatif spvp = pkcs_torval (pseq, 4);
    Relatif spvq = pkcs_torval (pseq, 5);
    Relatif crtp = pkcs_torval (pseq, 6);
    Relatif crtq = pkcs_torval (pseq, 7);
    Relatif crti = pkcs_torval (pseq, 8);
    // create a key vector argument
    Vector kvec;
    kvec.add (new Relatif (pmod));
    kvec.add (new Relatif (pexp));
    kvec.add (new Relatif (sexp));
    kvec.add (new Relatif (spvp));
    kvec.add (new Relatif (spvq));
    kvec.add (new Relatif (crtp));
    kvec.add (new Relatif (crtq));
    kvec.add (new Relatif (crti));
    // create the key
    return new Key (Key::CKEY_KRSA, kvec);
  }

  // get a pkcs rsa public key
  static Key* pkcs_torsa (const AsnBits* bits) {
    // check for a valid sequence
    if (bits == nullptr) return nullptr;
    // create an asn buffer by bit string
    AsnBuffer abuf (bits->tobuffer());
    // map the buffer as a node
    AsnNode* node = abuf.mapnode();
    // check for a sequence
    auto pseq = dynamic_cast <AsnSequence*> (node);
    // check the sequence length
    if (pseq->getnlen () != 2) {
      throw Exception ("pkcs-error", "invalid pkcs rsa sequence length");
    }
    // get the key element
    Relatif pmod = pkcs_torval (pseq, 0);
    Relatif pexp = pkcs_torval (pseq, 1);
    // create a key vector argument
    Vector kvec;
    kvec.add (new Relatif (pmod));
    kvec.add (new Relatif (pexp));
    // create the key
    return new Key (Key::CKEY_KRSA, kvec);
  }
  
  // get the pkcs key from the asn buffer
  static Key* pkcs_tokey (const AsnBuffer& abuf) {
    // map the buffer to a node
    AsnNode* node = abuf.mapnode ();
    // check for a sequence
    auto pseq = dynamic_cast <AsnSequence*> (node);
    if (pseq == nullptr) {
      delete node;
      throw Exception ("pkcs-error", "cannot map pkcs sequence node");
    }
    // check the sequence length 3 (private) 2 (public)
    if ((pseq->getnlen () != 3) && (pseq->getnlen () != 2)) {
      delete node;
      throw Exception ("pkcs-error", "invalid pkcs sequence length");
    }
    bool pflg = (pseq->getnlen () == 3) ? true : false;
    // get the object version in private mode
    if (pflg == true) {
      long vers = pkcs_torval(pseq, 0).tolong ();
      if (vers != 0L) {
	throw Exception ("pkcs-error", "invalid pkcs key version");
      }
    }
    long sidx = pflg ? 1L : 0L;
    // get the asn sequence
    auto aseq = dynamic_cast <AsnSequence*> (pseq->getnode(sidx));
    if (aseq == nullptr) {
      delete node;
      throw Exception ("pkcs-error", "cannot map pkcs sequence node");
    }
    if (aseq->getnlen () != 2) {
      delete node;
      throw Exception ("pkcs-error", "invalid pkcs sequence length");
    }
    // get the object oid
    auto soid = dynamic_cast <AsnOid*> (aseq->getnode (0));
    if (soid == nullptr) {
      delete node;
      throw Exception ("pkcs-error", "invalid pkcs object as oid");
    }
    Xoid::t_toid toid = Xoid::totoid (soid->getoid());
    // map the result key
    Key* result = nullptr;
    if (toid == Xoid::TLS_ALGO_RSAE) {
      // check for null node
      auto anil = dynamic_cast<AsnNull*>(aseq->getnode(1));
      if (anil == nullptr) {
	delete node;
	throw Exception ("pkcs-error", "missing null node in pkcs sequence");
      }
      // get the octets string
      result = pflg
	? pkcs_torsa (dynamic_cast<AsnOctets*>(pseq->getnode(2)))
	: pkcs_torsa (dynamic_cast<AsnBits*>  (pseq->getnode(1)));
    }
    delete node;
    return result;
  }

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default pkcs key
  
  Pkcs::Pkcs (void) {
    p_pkey = nullptr;
    reset ();
  }

  // create a pkcs key by key

  Pkcs::Pkcs (Key* pkey) {
    if (pkey != nullptr) {
      d_pemc = pkey->ispublic () ? Pem::PEMC_KEYK : Pem::PEMC_KEYP;
    }
    Object::iref (p_pkey = pkey);
  }

  // create a pkcs key by path

  Pkcs::Pkcs (const String& path) {
    // preset objects
    p_pkey = nullptr;
    // read by path
    Pki::read (path);
    // adjust key type
    if (p_pkey != nullptr) {
      d_pemc = p_pkey->ispublic () ? Pem::PEMC_KEYK : Pem::PEMC_KEYP;
    }
  }
  
  // copy construct this pkcs key

  Pkcs::Pkcs (const Pkcs& that) {
    that.rdlock ();
    try {
      // assign base object
      Pki::operator = (that);
      // copy locally
      p_pkey = (that.p_pkey == nullptr) ? nullptr : new Key (*that.p_pkey);
      Object::iref (p_pkey);
      // here it is
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }
      
  // destroy this pkcs key

  Pkcs::~Pkcs (void) {
    Object::dref (p_pkey);
  }

  // assign a pkcs key to this one

  Pkcs& Pkcs::operator = (const Pkcs& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      // assign base object
      Pki::operator = (that);
      // copy locally
      Object::dref (p_pkey);
      p_pkey = (that.p_pkey == nullptr) ? nullptr : new Key (*that.p_pkey);
      Object::iref (p_pkey);
      // here it is
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }
  
  // return the object class name

  String Pkcs::repr (void) const {
    return "Pkcs";
  }

  // return a clone of this object

  Object* Pkcs::clone (void) const {
    return new Pkcs (*this);
  }
  
  // reset the pkcs key

  void Pkcs::reset (void) {
    wrlock ();
    try {
      // reset base object
      Pki::reset ();
      // reset locally
      Object::dref (p_pkey); p_pkey = nullptr;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the pkcs key

  Key* Pkcs::getkey (void) const {
    rdlock ();
    try {
      Key* result = p_pkey;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - protected section                                                     -
  // -------------------------------------------------------------------------

  // encode the pkcs buffer

  bool Pkcs::encode (void) {
    wrlock ();
    try {
      bool result = false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // decode the pkcs buffer
  
  bool Pkcs::decode (void) {
    wrlock ();
    try {
      // create an asn buffer
      AsnBuffer abuf (d_xbuf);
      // map the buffer to a key
      Object::iref (p_pkey = pkcs_tokey (abuf));
      unlock ();
      return true;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 1;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_GETKEY = zone.intern ("get-key");

  // create a new object in a generic way
 
  Object* Pkcs::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // create a default object
    if (argc == 0) return new Pkcs;
    // check for 1 argument
    if (argc == 1) {
      Object* obj = argv->get (0);
      // check for a string
      String* path = dynamic_cast <String*> (obj);
      if (path != nullptr) return new Pkcs (*path);
      // check for a key
      Key* key = dynamic_cast <Key*> (obj);
      if (key != nullptr) return new Pkcs (key);
      // invalid object
      throw Exception ("type-error", "invalif object with pkcs",
		       Object::repr (obj));
    }
    // too many arguments
    throw Exception ("argument-error",
                     "too many argument with pkcs constructor");
  }

  // return true if the given quark is defined

  bool Pkcs::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Pki::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark

  Object* Pkcs::apply (Evaluable* zobj, Nameset* nset, const long quark,
		       Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETKEY) {
	rdlock ();
	try {
	  Object* result = getkey ();
	  zobj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
    }
    // call the pkcs method
    return Pki::apply (zobj, nset, quark, argv);
  }
}
