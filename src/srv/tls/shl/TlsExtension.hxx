// ---------------------------------------------------------------------------
// - TlsExtension.hxx                                                        -
// - afnix:tls service - tls content types definition                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TLSEXTENSION_HXX
#define  AFNIX_TLSEXTENSION_HXX

#ifndef  AFNIX_STRVEC_HPP
#include "Strvec.hpp"
#endif

#ifndef  AFNIX_CRYPTO_HPP
#include "Crypto.hpp"
#endif

namespace afnix {

  // the supported extension code
  static const t_word PV_EXT_0000 =  0; // server name
  static const t_word PV_EXT_0013 = 13; // signature and hash

  // convert a byte to a hash code
  static Crypto::t_hash tls_extn_hash (const t_byte code) {
    if (code == 0x00U) return Crypto::HASH_NIL;
    else if (code == 0x01U) return Crypto::HASH_MD5;
    else if (code == 0x02U) return Crypto::HASH_SHA1;
    else if (code == 0x03U) return Crypto::HASH_SHA224;
    else if (code == 0x04U) return Crypto::HASH_SHA256;
    else if (code == 0x05U) return Crypto::HASH_SHA384;
    else if (code == 0x06U) return Crypto::HASH_SHA512;
    throw Exception ("tls-error", "invalid hash code to decode");
  }

  // convert a byte to a signature code
  static Crypto::t_sign tls_extn_sign (const t_byte code) {
    if (code == 0x00U) return Crypto::SIGN_NIL;
    else if (code == 0x01U) return Crypto::SIGN_RSA;
    else if (code == 0x02U) return Crypto::SIGN_DSA;
    throw Exception ("tls-error", "invalid sign code to decode");
  }
  
  // the base extension structure
  struct s_extn {
    // the extension code
    t_word d_code;
    // create a nil extension
    s_extn (void) {
      d_code = PV_EXT_0000;
    }
  };

  // 0000: server name
  struct s_0000 : s_extn {
    // the server list
    Strvec d_slst;
    // create a default extension
    s_0000 (void) {
      d_code = PV_EXT_0000;
    }
    // create an extension by data
    s_0000 (const long size, const t_byte* data) {
      d_code = PV_EXT_0000;
      decode (size, data);
    }
    void decode (const long size, const t_byte* data) {
      if (size < 2) {
	throw Exception ("tls-error", "invalid extension size to decode");
      }
      // get the extension block size
      long boff = 0L;
      long bsiz = ((long) data[boff++] << 8); bsiz += ((long) data[boff++]);
      if (bsiz + 2L != size) {
	throw Exception ("tls-error", "inconsistent extension data size");
      }
      if (bsiz == 0L) return;
      // build the extension array
      while (boff < size) {
	// get the server name type
	t_byte code = data[boff++];
	// check for host name
	if (code == 0) {
	  // the the server name size
	  long nsiz = ((long) data[boff++] << 8); nsiz += ((long) data[boff++]);
	  // collect name data
	  char name[nsiz+1];
	  for (long k = 0L; k < nsiz; k++) name[k] = data[boff++];
	  name[nsiz] = nilc;
	  // add the server name
	  d_slst.add (name);
	} else {
	  throw Exception ("tls-error", "invalid extensionserver name code");
	}
      }
      if (boff != size) {
	throw Exception ("tls-error", "inconsistent server name size");
      }
    }
  };
  
  // 0013: signature and hash algorithm
  struct s_0013 : s_extn {
    // the hasher code
    Crypto::t_hash d_hash;
    // the signer code
    Crypto::t_sign d_sign;
    // create a default extension
    s_0013 (void) {
      d_code = PV_EXT_0013;
      d_hash = Crypto::HASH_SHA256;
      d_sign = Crypto::SIGN_RSA;
    }
    // create an extension by data
    s_0013 (const long size, const t_byte* data) {
      d_code = PV_EXT_0013;
      d_hash = Crypto::HASH_NIL;
      d_sign = Crypto::SIGN_NIL;
      decode (size, data);
    }
    // decode an extension by data
    void decode (const long size, const t_byte* data) {
      if (size != 2) {
	throw Exception ("tls-error", "invalid extension size to decode");
      }
      d_hash = tls_extn_hash (data[0]);
      d_sign = tls_extn_sign (data[1]);
    }
  };

  // decode an extension by code and data
  static s_extn* tls_extn_xx (const t_word code,
			      const long size, const t_byte* data) {
    // check for nil
    if ((size == 0L) || (data == nullptr)) return nullptr;
    // check for extension codes
    if (code == PV_EXT_0000) return new s_0000 (size, data);
    if (code == PV_EXT_0013) return new s_0013 (size, data);
    // invalid code
    throw Exception ("tls-error", "invalid extension to decode");
  }
}

#endif
