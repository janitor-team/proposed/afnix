// ---------------------------------------------------------------------------
// - Pkcs.hpp                                                                -
// - afnix:tls service - public key cryptographic standard class definition  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2020 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PKCS_HPP
#define  AFNIX_PKCS_HPP

#ifndef  AFNIX_PKI_HPP
#include "Pki.hpp"
#endif

#ifndef  AFNIX_KEY_HPP
#include "Key.hpp"
#endif

namespace afnix {

  /// The Pkcs is a cryptography standard originally defined by RSA Security
  /// Inc. This implementation corresponds to PKCS#8, which is now a standard
  /// track referenced as RFC5208 and updated by RFC5958. This standard
  /// expands RFC 3447 with keys of different natures.
  /// @author amaury darsch

  class Pkcs : public Pki {
  private:
    /// the pkcs key
    Key* p_pkey;

  public:
    /// create a default pkcs key
    Pkcs (void);

    /// create a pkcs by key
    /// @param pkey the key to set
    Pkcs (Key* pkey);
    
    /// create a pkcs key by path
    /// @param path the input file path
    Pkcs (const String& path);
    
    /// copy construct a pkcs key object
    /// @param that the object to copy
    Pkcs (const Pkcs& that);

    /// destroy this pkcs key
    ~Pkcs (void);
    
    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;

    /// assign a pkcs key to this one
    /// @param that the object to copy
    Pkcs& operator = (const Pkcs& that);

    /// reset this pkcs object
    void reset (void);

    /// @return the pkcs key
    virtual Key* getkey (void) const;

  protected:
    /// encode a pki buffer
    bool encode (void);

    /// decode a pki buffer
    bool decode (void);
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;
    
    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
