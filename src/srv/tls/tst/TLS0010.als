# ---------------------------------------------------------------------------
# - TLS0010.als                                                             -
# - afnix:tls module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2020 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   pkcs-8 test unit
# @author amaury darsch

# get the module
interp:library "afnix-sec"
interp:library "afnix-tls"

# create a pkcs key by path
const pder (afnix:tls:Pkcs "KEY0010.der")
const ppem (afnix:tls:Pkcs "KEY0010.pem")

# check type
assert afnix:tls:Pem:PUBLIC-KEY (pder:get-type)
assert afnix:tls:Pem:PUBLIC-KEY (ppem:get-type)

# compare keys
const kder (pder:get-key)
const kpem (ppem:get-key)
assert afnix:sec:Key:KRSA (kder:get-type)
assert afnix:sec:Key:KRSA (kpem:get-type)

assert (kder:get-size) (kpem:get-size)
assert (kder:get-bits) (kpem:get-bits)

trans rder (kder:get-relatif-key afnix:sec:Key:RSA-MODULUS)
trans rpem (kpem:get-relatif-key afnix:sec:Key:RSA-MODULUS)
assert rder rpem

trans rder (kder:get-relatif-key afnix:sec:Key:RSA-PUBLIC-EXPONENT)
trans rpem (kpem:get-relatif-key afnix:sec:Key:RSA-PUBLIC-EXPONENT)
assert rder rpem

# compare with the original private key
const porg  (afnix:tls:Pkcs "KEY0009.pem")
const korg  (porg:get-key)

trans rder (kder:get-relatif-key afnix:sec:Key:RSA-MODULUS)
trans rpem (korg:get-relatif-key afnix:sec:Key:RSA-MODULUS)
assert rder rpem

trans rder (kder:get-relatif-key afnix:sec:Key:RSA-PUBLIC-EXPONENT)
trans rpem (korg:get-relatif-key afnix:sec:Key:RSA-PUBLIC-EXPONENT)
assert rder rpem
