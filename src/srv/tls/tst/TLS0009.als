# ---------------------------------------------------------------------------
# - TLS0009.als                                                             -
# - afnix:tls module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2020 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   pkcs test unit
# @author amaury darsch

# get the module
interp:library "afnix-sec"
interp:library "afnix-tls"

# create a pkcs key by path
const pder (afnix:tls:Pkcs "KEY0009.der")
const ppem (afnix:tls:Pkcs "KEY0009.pem")

# check type
assert afnix:tls:Pem:PRIVATE-KEY (pder:get-type)
assert afnix:tls:Pem:PRIVATE-KEY (ppem:get-type)

# compare keys
const kder (pder:get-key)
const kpem (ppem:get-key)
assert afnix:sec:Key:KRSA (kder:get-type)
assert afnix:sec:Key:KRSA (kpem:get-type)

assert (kder:get-size) (kpem:get-size)
assert (kder:get-bits) (kpem:get-bits)

trans rder (kder:get-relatif-key afnix:sec:Key:RSA-MODULUS)
trans rpem (kpem:get-relatif-key afnix:sec:Key:RSA-MODULUS)
assert rder rpem

trans rder (kder:get-relatif-key afnix:sec:Key:RSA-PUBLIC-EXPONENT)
trans rpem (kpem:get-relatif-key afnix:sec:Key:RSA-PUBLIC-EXPONENT)
assert rder rpem

trans rder (kder:get-relatif-key afnix:sec:Key:RSA-SECRET-EXPONENT)
trans rpem (kpem:get-relatif-key afnix:sec:Key:RSA-SECRET-EXPONENT)
assert rder rpem
