# ---------------------------------------------------------------------------
# - CSM0031.als                                                             -
# - afnix:csm service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2021 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   datum test unit
# @author amaury darsch

# get the service
interp:library "afnix-csm"

# create a nil datum
trans dtum (afnix:csm:Datum)

# check predicate and representation
assert true   (afnix:csm:datum-p dtum)
assert "Datum" (dtum:repr)

# check mode
assert afnix:csm:Datum:NONE (dtum:get-mode)

# bind and check
dtum:bind afnix:csm:Datum:CONSTANT 1
assert afnix:csm:Datum:CONSTANT (dtum:get-mode)
assert 1 (dtum:get-value)

