# ---------------------------------------------------------------------------
# - CSM0032.als                                                             -
# - afnix:csm service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2021 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   mixture test unit
# @author amaury darsch

# get the service
interp:library "afnix-csm"

# create a nil mixture
trans mixt (afnix:csm:Mixture)

# check predicate and representation
assert true   (afnix:csm:mixture-p mixt)
assert "Mixture" (mixt:repr)

# check content
assert 0 (mixt:length)

# create a datum
const dtum (afnix:csm:Datum "datum" "datum part")
dtum:bind afnix:csm:Datum:CONSTANT 0

# add and check content
mixt:add dtum
assert 1 (mixt:length)

# get and check
trans mdtm  (mixt:get 0)
assert true (afnix:csm:datum-p mdtm)
assert 0    (mdtm:get-value)
assert "datum" (mdtm:get-name)

# find and check
trans mdtm  (mixt:find "datum")
assert true (afnix:csm:datum-p mdtm)
assert 0    (mdtm:get-value)
assert "datum" (mdtm:get-name)
