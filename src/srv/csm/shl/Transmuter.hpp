// ---------------------------------------------------------------------------
// - Transmuter.hpp                                                          -
// - afnix:csm service - mixture transmutation class definition                -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TRANSMUTER_HPP
#define  AFNIX_TRANSMUTER_HPP

#ifndef  AFNIX_MIXTURE_HPP
#include "Mixture.hpp"
#endif

namespace afnix {

  /// The Transmuter class is a mixture transformation interface. Given an
  /// input mixture, the 'process' method generates an output mixture. The
  /// transmuter configuration is left to the derived interface.
  /// @author amaury darsch

  class Transmuter : public Object {
  public:
    /// reset this transmuter
    virtual void reset (void) =0;

    /// stage this transmuter by micture
    virtual bool stage (const Mixture& mixt) =0;

    /// process a staged transmuter
    virtual Mixture process (void) =0;
    
    /// process a mixture
    /// @param mixt the input mixture
    virtual Mixture process (const Mixture& mixt);
    
  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;
    
    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments  to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
                   Vector* argv) override;
  };
}

#endif
