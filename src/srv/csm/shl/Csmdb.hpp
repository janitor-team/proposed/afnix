// ---------------------------------------------------------------------------
// - Csmdb.hpp                                                               -
// - afnix:csm service - part/blob plist name definitions                    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_CSMDB_HPP
#define  AFNIX_CSMDB_HPP

#ifndef  AFNIX_STRING_HPP
#include "String.hpp"
#endif

namespace afnix {

  // the part plist name/info
  static const String PN_PRT_NAME = "PN-PRT-NAME";
  static const String PI_PRT_NAME = "PART NAME";
  static const String PN_PRT_INFO = "PN-PRT-INFO";
  static const String PI_PRT_INFO = "PART INFO";
  static const String PN_PRT_UUID = "PN-PRT-UUID";
  static const String PI_PRT_UUID = "PART UUID";
  static const String PN_PRT_PLST = "PN-PRT-PLST";
  static const String PI_PRT_PLST = "PART PLIST";

  // the mixture plist name/info
  static const String PN_MIX_NAME = "PN-MIX-NAME";
  static const String PI_MIX_NAME = "MIXTURE NAME";
  static const String PN_MIX_INFO = "PN-MIX-INFO";
  static const String PI_MIX_INFO = "MIXTURE INFO";
  static const String PN_MIX_UUID = "PN-MIX-UUID";
  static const String PI_MIX_UUID = "MIXTURE UUID";
  static const String PN_MIX_PLST = "PN-MIX-PLST";
  static const String PI_MIX_PLST = "MIXTURE PLIST";
  static const String PN_MIX_LGTH = "PN-MIX-LGTH";
  static const String PI_MIX_LGTH = "MIXTURE LENGTH";
  
  // the domain plist name/info
  static const String PN_DMN_NAME = "PN-DMN-NAME";
  static const String PI_DMN_NAME = "DOMAIN NAME";
  static const String PN_DMN_INFO = "PN-DMN-INFO";
  static const String PI_DMN_INFO = "DOMAIN INFO";
  static const String PN_DMN_XRID = "PN-DMN-XRID";
  static const String PI_DMN_XRID = "DOMAIN RID";
  static const String PN_DMN_CTIM = "PN-DMN-CTIM";
  static const String PI_DMN_CTIM = "DOMAIN CREATION TIME";
  static const String PN_DMN_MTIM = "PN-DMN-MTIM";
  static const String PI_DMN_MTIM = "DOMAIN MODIFICATION TIME";

  // the blob plist name/info
  static const String PN_BLB_NAME = "PN-BLB-NAME";
  static const String PI_BLB_NAME = "BLOB NAME";
  static const String PN_BLB_INFO = "PN-BLB-INFO";
  static const String PI_BLB_INFO = "BLOB INFO";
  static const String PN_BLB_UUID = "PN-BLB-UUID";
  static const String PI_BLB_UUID = "BLOB UUID";
  static const String PN_BLB_PLST = "PN-BLB-PLST";
  static const String PI_BLB_PLST = "BLOB PLIST";
  static const String PN_BLB_TYPE = "PN-BLB-TYPE";
  static const String PI_BLB_TYPE = "BLOB TYPE";
  static const String PN_BLB_XRID = "PN-BLB-XRID";
  static const String PI_BLB_XRID = "BLOB RID";
  static const String PN_BLB_CTIM = "PN-BLB-CTIM";
  static const String PI_BLB_CTIM = "BLOB CREATION TIME";
  static const String PN_BLB_MTIM = "PN-BLB-MTIM";
  static const String PI_BLB_MTIM = "BLOB MODIFICATION TIME";
  static const String PN_BLB_TFLG = "PN-BLB-TFLG";
  static const String PI_BLB_TFLG = "BLOB TRANSIENT FLAG";
}

#endif
