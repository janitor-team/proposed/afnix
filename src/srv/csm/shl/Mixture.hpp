// ---------------------------------------------------------------------------
// - Mixture.hpp                                                             -
// - afnix:csm service - datum mixture class definition                      -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_MIXTURE_HPP
#define  AFNIX_MIXTURE_HPP

#ifndef  AFNIX_DATUM_HPP
#include "Datum.hpp"
#endif

#ifndef  AFNIX_COLLECTION_HPP
#include "Collection.hpp"
#endif

namespace afnix {

  /// The Mixture class is a part that encapsulates a collection of datum.
  /// The mixture serves as an object interface with various services.
  /// @author amaury darsch

  class Mixture : public Part {
  protected:
    /// the datum collection
    Collection* p_cdtm;
    
  public:
    /// create a mixture
    Mixture (void);

    /// create a mixture by name
    /// @param name the mixture name
    Mixture (const String& name);

    /// create a mixture by name and info
    /// @param name the mixture name
    /// @param info the mixture info
    Mixture (const String& name, const String& info);

    /// copy construct this mixture
    /// @param that the mixture to copy
    Mixture (const Mixture& that);

    /// copy move this mixture
    /// @param that the mixture to move
    Mixture (Mixture&& that) noexcept;
    
    /// destroy this mixture
    ~Mixture (void);

    /// assign a mixture to this one
    /// @param that the mixture to assign
    Mixture& operator = (const Mixture& that);

    /// move a mixture to this one
    /// @param that the mixture to move
    Mixture& operator = (Mixture&& that) noexcept;
    
    /// @return the class name
    String repr (void) const override;

    /// @return a clone of this object
    Object* clone (void) const override;

    /// @return the serial did
    t_word getdid (void) const override;

    /// @return the serial sid
    t_word getsid (void) const override;
    
    /// serialize this blob
    /// @param os the output stream
    void wrstream (OutputStream& os) const override;

    /// deserialize this blob
    /// @param is the input stream
    void rdstream (InputStream& os) override;

    /// @return the object plist
    Plist getplst (void) const override;

    /// @return a mixture object view
    HashTable toview (void) const override;

    /// @return a subpart by name
    Part* subpart (const String& name) const override;

    /// @return the number of datum
    virtual long length (void) const;

    /// add a datum to the collection
    /// @param dtum the datum to add
    virtual void add (Datum* dtum);

    /// @return a datum by index
    virtual Datum* get (const long didx) const;
    
    /// @return a datum by name
    virtual Datum* find (const String& name) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;
    
    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments  to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
                   Vector* argv) override;
  };
}

#endif
