// ---------------------------------------------------------------------------
// - Voxel.hpp                                                               -
// - afnix:dip service - image voxel class definition                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_VOXEL_HPP
#define  AFNIX_VOXEL_HPP

#ifndef  AFNIX_IMAGE_HPP
#include "Image.hpp"
#endif

#ifndef  AFNIX_PIXEL_HPP
#include "Pixel.hpp"
#endif

namespace afnix {
  
  /// The Voxel class is a single or multiple band image class which
  /// aggregates a block of pixels. The voxel is characterized by its
  /// depth. The voxel format is defined by the pixel format which is
  /// the same across the voxel texels.
  /// @author amaury darsch

  class Voxel : public Image {
  protected:
    /// the pixel format
    Pixel::t_pfmt d_pfmt;
    /// the voxel depth
    long d_dpth;
    
  public:
    /// create a default voxel
    Voxel (void);

    /// copy construct this voxel
    /// @param that the voxel to copy
    Voxel (const Voxel& that);

    /// copy move this voxel
    /// @param that the voxel to move
    Voxel (Voxel&& that) noexcept;

    /// assign an voxel to this one
    /// @param that the voxel to assign
    Voxel& operator = (const Voxel& that);

    /// move an voxel to this one
    /// @param that the voxel to move
    Voxel& operator = (Voxel&& that) noexcept;

    // serialize this voxel
    /// @param os the output stream
    void wrstream (OutputStream& os) const;

    /// deserialize this voxel
    /// @param is the input stream
    void rdstream (InputStream& os);
    
    /// reset this voxel
    void reset (void);
    
    /// @return the pixel format
    virtual Pixel::t_pfmt getpfmt (void) const;
    
    /// @return the voxel depth
    virtual long getdpth (void) const;
    
    /// set an voxel component by position and pixel
    /// @param x the voxel x position
    /// @param y the voxel y position
    /// @param d the voxel d position
    /// @param pixl the pixel value
    virtual void setpixl (const long x, const long y, const long d,
			  const Pixel& pixl) =0;

    /// @return a pixel by position
    virtual Pixel getpixl (const long x, const long y, const long d) const =0;
    
  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
