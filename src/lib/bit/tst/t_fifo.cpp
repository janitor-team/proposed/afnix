// ---------------------------------------------------------------------------
// - t_fifo.cpp                                                              -
// - afnix nuts and bolts - fifo tester                                      -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "fifo.tcc"

int main (int, char**) {
  using namespace afnix;

  // create a new fifo
  t_fifo<long> fifo;
  if (fifo.empty () == false) return 1;
  if (fifo.full  () == false) return 1;
  if (fifo.length() != 0L)    return 1;

  // resize and test
  fifo.resize (10L);
  if (fifo.empty () == false) return 1;
  if (fifo.full  () == true)  return 1;
  if (fifo.length() != 0L)    return 1;

  // push and test
  for (long k = 0L; k < 10; k++) fifo.push (k);
  if (fifo.empty () == true)  return 1;
  if (fifo.full  () == false) return 1;
  if (fifo.length() != 10L)   return 1;

  // pop and test
  for (long k = 0L; k < 10; k++) {
    long val = fifo.pop ();
    if (val != k) return 1;
  }
  if (fifo.empty () == false) return 1;
  if (fifo.full  () == true)  return 1;
  if (fifo.length() != 0L)    return 1;

  // push and drop
  for (long k = 0L; k < 11; k++) fifo.push (k);
  if (fifo.empty () == true)  return 1;
  if (fifo.full  () == false) return 1;
  if (fifo.length() != 10L)   return 1;
  for (long k = 0L; k < 10; k++) {
    long val = fifo.pop ();
    if (val != (k+1)) return 1;
  }
  if (fifo.empty () == false) return 1;
  if (fifo.full  () == true)  return 1;
  if (fifo.length() != 0L)    return 1;
  // done
  return 0;
}
