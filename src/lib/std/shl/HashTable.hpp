// ---------------------------------------------------------------------------
// - HashTable.hpp                                                           -
// - standard object library - hash table object class definition            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_HASHTABLE_HPP
#define  AFNIX_HASHTABLE_HPP

#ifndef  AFNIX_STRING_HPP
#include "String.hpp"
#endif

namespace afnix {

  /// The HashTable class is another container class which maps an object
  /// with a key. The hash table is dynamic and get resized automatically
  /// when needed. When an object is added, the object reference count is
  /// increased. When the object is retreived, the reference count is not
  /// touched. The lookup method throw an exception if the key is not found.
  /// The get method returns nullptr if the object is not found. The table can
  /// be configured to operate in a case insensitive way. If the case flag
  /// is changed, the table is automatically reset.
  /// @author amaury darsch

  class HashTable : public virtual Serial {
  private:
    /// the hash table size
    long d_size;
    /// the hash table length
    long d_hlen;
    /// the hash table threshold
    long d_thrs;
    /// the case insensitive flag
    bool d_cifg;
    /// array of buckets
    struct s_bucket** p_htbl;

  public:
    /// create a hash table with a default size
    HashTable (void);

    /// create a hash table with a case flag
    /// @param cifg the case insensitive flag
    HashTable (const bool cifg);

    /// create a hash table with an initial size
    /// @param size the hash table size
    HashTable (const long size);

    /// create a hash table with an initial size and case
    /// @param size the hash table size
    /// @param cifg the case insensitive flag
    HashTable (const long size, const bool cifg);

    /// copy construct this hash table
    /// @param that the table to copy 
    HashTable (const HashTable& that);

    /// copy move this hash table
    /// @param that the table to move 
    HashTable (HashTable&& that) noexcept;
    
    /// destroy the hash table. 
    ~HashTable (void);

    /// assign a hash table to this one
    /// @param that the table to assign
    HashTable& operator = (const HashTable& that);

    /// move a hash table to this one
    /// @param that the table to move
    HashTable& operator = (HashTable&& that) noexcept;
    
    /// return the class name
    String repr (void) const override;

    /// @return a clone of this object
    Object* clone (void) const override;
    
    /// @return the hash table did
    t_word getdid (void) const override;

    /// @return the hash table sid
    t_word getsid (void) const override;

    /// serialize this hash table to an output stream
    /// @param os the output stream to write
    void wrstream (class OutputStream& os) const override;

    /// deserialize a hash table from an input stream
    /// @param is the input steam to read in
    void rdstream (class InputStream& is) override;
    
    /// reset the hash table
    virtual void reset (void);

    /// @return the number of elements
    virtual long length (void) const;

    /// @return true if the table is empty
    virtual bool empty (void) const;

    /// set the case insensitive flag
    /// @param cifg the case flag to set
    virtual void setcifg (const bool cifg);

    /// @return the case insensitive flag
    virtual bool getcifg (void) const;

    /// @return the element key by index
    virtual String getkey (const long index) const;

    /// @return the element object by index
    virtual Object* getobj (const long index) const;
  
    /// @return true if the key exists in this table
    virtual bool exists (const String& key) const;

    /// set or create a new object in the table.
    /// @param key the object key to set
    /// @param obj the object to set or add
    virtual void add (const String& key, Object* obj);

    /// get an object by key. If the object is not found nil is returned.
    /// this means that this function always succeds. You can use lookup 
    /// instead to get an exception if the key is not found. You can also use
    /// the exists method to check for a key but exists and get is very slow.
    /// @param key the object key to search
    virtual Object* get (const String& key) const;

    /// find an object by key or throw an exception
    /// @param key the object key to find
    virtual Object* lookup (const String& key) const;

    /// remove an object in the hash table. 
    /// @param key the object key to remove
    virtual void remove (const String& key);

    /// @return a vector of keys
    virtual Vector* getkeys (void) const;

    /// @return a vector of objects
    virtual Vector* getvobj (void) const;

    /// resize this hash table
    virtual void resize (const long size);

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv) override;
  };
}

#endif
