// ---------------------------------------------------------------------------
// - Fifo.cpp                                                                -
// - standard object library - fifo class implementation                     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Fifo.hpp"
#include "Vector.hpp"
#include "Integer.hpp"
#include "Boolean.hpp"
#include "Evaluable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "fifo.tcc"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------
  
  // create a new fifo
  
  Fifo::Fifo (void) {
    p_fifo = new t_fifo<Object*>;
  }
  
  // create a new fifo by size
  
  Fifo::Fifo (const long size) {
    p_fifo = new t_fifo<Object*>(size);
  }

  // copy construct this fifo
  
  Fifo::Fifo (const Fifo& that) {
    that.rdlock ();
    try {
      // allocate the fifo
      p_fifo = new t_fifo<Object*>;
      // copy by getter
      long flen = that.p_fifo->length (); p_fifo->resize (flen);
      for (long k = 0L; k < flen; k++) {
	p_fifo->push (Object::iref (that.p_fifo->get(k)));
      }
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }

  // destroy this fifo

  Fifo::~Fifo (void) {
    while (p_fifo->empty() == false) Object::dref (p_fifo->pop ());
    delete p_fifo;
  }

  // return the class name

  String Fifo::repr (void) const {
    return "Fifo";
  }

  // assign a fifo to this one

  Fifo& Fifo::operator = (const Fifo& that) {
    // check for self assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      // remove the old fifo
      while (p_fifo->empty() == false) Object::dref (p_fifo->pop());
      delete p_fifo; p_fifo = nullptr;
      // allocate the fifo
      p_fifo = new t_fifo<Object*>;
      // copy by getter
      long flen = that.p_fifo->length (); p_fifo->resize (flen);
      for (long k = 0L; k < flen; k++) {
	p_fifo->push (Object::iref (that.p_fifo->get(k)));
      }
      // unlock and return
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }

  // reset this fifo class

  void Fifo::reset (void) {
    wrlock ();
    try {
      while (p_fifo->empty() == false) Object::dref (p_fifo->pop());
      p_fifo->reset ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the fifo size

  long Fifo::getsize (void) const {
    rdlock ();
    try {
      long result = p_fifo->getsize ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the fifo length

  long Fifo::length (void) const {
    rdlock ();
    try {
      long result = p_fifo->length ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // return true if the fifo is empty

  bool Fifo::empty (void) const {
    rdlock ();
    try {
      bool result = p_fifo->empty ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // return true if the fifo is full

  bool Fifo::full (void) const {
    rdlock ();
    try {
      bool result = p_fifo->full ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // push an object into the fifo

  void Fifo::push (Object* obj) {
    // lock and push
    wrlock ();
    try {
      // check for full (but not empty ah ah)
      if ((p_fifo->empty() == false) && (p_fifo->full () == true)) {
	Object::dref (p_fifo->pop ());
      }
      // push the object
      p_fifo->push (Object::iref (obj));
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // pop an object from the fifo

  Object* Fifo::pop (void) {
    wrlock ();
    try {
      // check for empty
      if (p_fifo->empty () == true) {
	unlock ();
	return nullptr;
      }
      Object* result = p_fifo->pop ();
      Object::tref (result);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a fifo object by index

  Object* Fifo::get (const long index) const {
    rdlock ();
    try {
      // check that we are bounded
      if ((index < 0) || (index >= p_fifo->length ())) {
	throw Exception ("index-error","fifo index is out of range");
      }
      // get the object and unlock
      Object* result = p_fifo->get (index);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // resize the fifo

  void Fifo::resize (const long size) {
    wrlock ();
    try {
      p_fifo->resize (size);
      unlock ();	
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 9;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_GET     = zone.intern ("get");
  static const long QUARK_POP     = zone.intern ("pop");
  static const long QUARK_PUSH    = zone.intern ("push");
  static const long QUARK_RESET   = zone.intern ("reset");
  static const long QUARK_FULLP   = zone.intern ("full-p");
  static const long QUARK_EMPTYP  = zone.intern ("empty-p");
  static const long QUARK_RESIZE  = zone.intern ("resize");
  static const long QUARK_LENGTH  = zone.intern ("length");
  static const long QUARK_GETSIZE = zone.intern ("get-size");
  
  // create a new object in a generic way

  Object* Fifo::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // check 0 argument
    if (argc == 0) return new Fifo;
    // check 1 argument
    if (argc == 1) {
      long size = argv->getlong (0);
      return new Fifo (size);
    }
    throw Exception ("argument-error", "too many argument for fifo");
  }

  // return true if the given quark is defined
  
  bool Fifo::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Object::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object class with a set of arguments and a quark
  
  Object* Fifo::apply (Evaluable* zobj, Nameset* nset, const long quark,
		       Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_FULLP)   return new Boolean (full    ());
      if (quark == QUARK_EMPTYP)  return new Boolean (empty   ());
      if (quark == QUARK_LENGTH)  return new Integer (length  ());
      if (quark == QUARK_GETSIZE) return new Integer (getsize ());
      if (quark == QUARK_RESET) {
	reset ();
	return nullptr;
      }
      if (quark == QUARK_POP) {
	wrlock ();
	try {
	  Object* result = pop ();
	  zobj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
    }
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_PUSH) {
	Object* obj = argv->get (0);
	push (obj);
	return nullptr;
      }
      if (quark == QUARK_GET) {
	long index = argv->getlong (0);
	rdlock ();
	try {
	  Object* result = get (index);
	  zobj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_RESIZE) {
	long size = argv->getlong (0);
	resize (size);
	return nullptr;
      }
    }
    // apply these arguments with the object
    return Object::apply (zobj, nset, quark, argv);
  }
}
