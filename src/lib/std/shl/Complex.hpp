// ---------------------------------------------------------------------------
// - Complex.hpp                                                             -
// - standard object library - complex class definition                      -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef AFNIX_COMPLEX_HPP
#define AFNIX_COMPLEX_HPP

#ifndef  AFNIX_REAL_HPP
#include "Real.hpp"
#endif

namespace afnix {

  /// The Complex class is a the object version of the native floating point
  /// number. The complex number is implemented like the integer class and
  /// is derived from the Literal class. The class implements also a bunch of
  /// method for floating point operations.
  /// @author amaury darsch

  class Complex : public Number {
  public:
    // the number format
    static const String CV_NUM_FRMT;

    /// compute the opposite of the complex
    /// @param x the complex to oppose
    /// @return a new complex opposite of the argument
    friend Complex operator - (const Complex& x);

    /// add two complex together
    /// @param x the first argument to add
    /// @param y the second argument to add
    /// @return a new sum of the previous one
    friend Complex operator + (const Complex& x, const Complex& y);

    /// subtract two complex together
    /// @param x the first argument to subtract
    /// @param y the second argument to subtract
    /// @return a new complex as the difference
    friend Complex operator - (const Complex& x, const Complex& y);

    /// multiply two complex together
    /// @param x the first argument to multiply
    /// @param y the second argument to multiply
    /// @return a new complex product
    friend Complex operator * (const Complex& x, const Complex& y);

    /// divide two complex together
    /// @param x the numerator
    /// @param y the denumerator
    /// @return the division of the arguments
    friend Complex operator / (const Complex& x, const Complex& y);

  private:
    /// the complex value
    t_real d_zval[2];

  public:
    /// create a new default complex
    Complex (void);

    /// create a new complex from a native value
    /// @param rval the real value
    Complex (const t_real rval);

    /// create a new complex from a native value
    /// @param rval the real value
    /// @param ival the imaginary value
    Complex (const t_real real, const t_real imag);

    /// create a new complex from an integer class
    /// @param lval the integer value
    Complex (const Integer& lval);

    /// create a new complex from a string
    /// @param value the string value
    Complex (const String& sval);

    /// copy constructor for this complex
    /// @param that the complex class to copy
    Complex (const Complex& that);

    /// @return the class name
    String repr (void) const override;

    /// @return a clone of this object
    Object* clone (void) const override;

    /// clear this complex
    void clear (void) override;

    /// @return a literal representation of this complex
    String toliteral (void) const override;

    /// @return a string representation of this complex
    String tostring (void) const override;

    /// @return a styled string of this complex
    String format (const Style& lstl) const override;

    /// @return the complex did
    t_word getdid (void) const override;

    /// @return the complex sid
    t_word getsid (void) const override;

    /// serialize this complex to an output stream
    /// @param os the output stream to write
    void wrstream (class OutputStream& os) const override;

    /// deserialize a complex from an input stream
    /// @param is the input steam to read in
    void rdstream (class InputStream& is) override;

    /// assign an complex with a native value
    /// @param rval the value to assign
    Complex& operator = (const t_real rval);

    /// assign an complex to this one
    /// @param that the complex to assign
    Complex& operator = (const Complex& that);

    /// add a complex to this one
    /// @param x the argument to add
    /// @return this added complex
    Complex& operator += (const Complex& x);

    /// substract a complex to this one
    /// @param x the argument to substract
    /// @return this added complex
    Complex& operator -= (const Complex& x);

    /// multiply a real with this one
    /// @param x the argument to multiply
    /// @return this multiplied complex
    Complex& operator *= (const t_real x);

    /// multiply a complex with this one
    /// @param x the argument to multiply
    /// @return this multiplied complex
    Complex& operator *= (const Complex& x);

    /// divide a real with this one
    /// @param x the argument to multiply
    /// @return this multiplied complex
    Complex& operator /= (const t_real x);

    /// multiply a complex with this one
    /// @param x the argument to multiply
    /// @return this multiplied complex
    Complex& operator /= (const Complex& x);

    /// compare this complex with a native value
    /// @param lval the value to compare
    /// @return true if they are equals
    bool operator == (const t_long lval) const;

    /// compare this complex with a native value
    /// @param lval the value to compare
    /// @return true if they are not equals
    bool operator != (const t_long lval) const;

    /// compare this complex with a native value
    /// @param rval the value to compare
    /// @return true if they are equals
    bool operator == (const t_real rval) const;

    /// compare this complex with a native value
    /// @param rval the value to compare
    /// @return true if they are not equals
    bool operator != (const t_real rval) const;

    /// compare two complex
    /// @param zval the value to compare
    /// @return true if they are equals
    bool operator == (const Complex& zval) const;

    /// compare two complex
    /// @param zval the value to compare
    /// @return true if they are not equals
    bool operator != (const Complex& zval) const;

    /// @return the number format
    String tofrmt (void) const override;

    /// @return true if this complex is zero
    bool iszero (void) const;

    /// @return the viewable size
    long tosize (void) const override;

    /// @return the viewable data
    t_byte* tobyte (void) override;

    /// @return the viewable data
    const t_byte* tobyte (void) const override;

    /// @return the real part
    virtual t_real getrval (void) const;

    /// @return the imaginary part
    virtual t_real getival (void) const;

    /// @rerurn the complex square module
    virtual t_real tosqm (void) const;

    /// @rerurn the complex module
    virtual t_real tomod (void) const;

    /// @rerurn the complex argument
    virtual t_real toarg (void) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// operate this object with another object
    /// @param type   the operator type
    /// @param object the operand object
    Object* oper (t_oper type, Object* object) override;

    /// set an object to this complex
    /// @param zobj   the current evaluable
    /// @param nset   the current nameset
    /// @param object the object to set
    Object* vdef (Evaluable* zobj, Nameset* nset, Object* object) override;

    /// apply this complex with a set of arguments and a quark
    /// @param zobj   the current evaluable
    /// @param nset   the current nameset    
    /// @param quark  the quark to apply these arguments
    /// @param argv   the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv) override;
  };
}

#endif
