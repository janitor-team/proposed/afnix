// ---------------------------------------------------------------------------
// - Common.cpp                                                              -
// - standard object library - global class implementation                   -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Common.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the debug flag
  #ifdef AFNIX_DEBUG
  static bool cv_cmn_dbug = true;
  #else
  static bool cv_cmn_dbug = false;
  #endif
  
  // the assert flag
  static bool cv_cmn_asrt = false;
  
  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // set the debug flag

  void Common::setdbug (const bool dbug) {
    cv_cmn_dbug = dbug;
  }

  // get the deug flag

  bool Common::getdbug (void) {
    return cv_cmn_dbug;
  }

  // set the assert flag

  void Common::setasrt (const bool asrt) {
    cv_cmn_asrt = asrt;
  }

  // get the assert flag

  bool Common::getasrt (void) {
    return cv_cmn_asrt;
  }
}
