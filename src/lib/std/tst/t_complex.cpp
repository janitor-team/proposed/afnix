// ---------------------------------------------------------------------------
// - t_real.cpp                                                              -
// - standard object library - real class tester module                      -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Complex.hpp"
#include "InputOutput.hpp"

int main (int, char**) {
  using namespace afnix;

  // create a default null real
  Complex z0;
  if (z0.iszero () == false) return 1;
  if ((z0.getrval () != 0.0) || (z0.getival () != 0.0)) return 1;

  // create a real/imgainary number
  Complex z1 (1.0);
  if (z1.getrval () != 1.0) return 1;
  if (z1.getival () != 0.0) return 1;
  Complex z2 (0.0, 1.0);
  if (z2.getrval () != 0.0) return 1;
  if (z2.getival () != 1.0) return 1;

  // check addition/substraction
  Complex z3 = z1 + z2;
  if (z3.getrval () != 1.0) return 1;
  if (z3.getival () != 1.0) return 1;
  Complex z4 = z3 - z1 - z2;
  if (z4.iszero () == false) return 1;
  if (z4 != z0) return 1;

  // check multiplcation
  z4 = Complex (1.0, -1.0);
  Complex z5 = z3 * z4;
  if (z5.getrval () != 2.0) return 1;
  if (z5.getival () != 0.0) return 1;

  // check division
  Complex z6 = z5 / z4;
  if (z6 != z3) return 1;
  z6 = z5 / z3;
  if (z6 != z4) return 1;
  
  // check string representation
  Complex z7 ("+1.0E+2-1.0E-2i");
  if (z7.getrval () != 100.0) return 1;
  if (z7.getival () != -0.01) return 1;
  
  // check serialization
  InputOutput io;
  z7.serialize (io);
  auto zs = dynamic_cast<Complex*> (Serial::deserialize (io));
  if (zs == nullptr) return 1;
  if (z7 != *zs) return 1;
  delete zs;
  
  // we are done
  return 0;
}
