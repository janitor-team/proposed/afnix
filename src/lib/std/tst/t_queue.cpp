// ---------------------------------------------------------------------------
// - t_queue.cpp                                                              -
// - standard object library - queue class tester                             -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Queue.hpp"

int main (int, char**) {
  using namespace afnix;

  // create a new queue
  Queue queue (2);
  String* hello  = new String ("hello");
  String* world  = new String ("world");

  Object::iref (hello);
  Object::iref (world);

  // check size and predicates
  if (queue.getsize () != 2)     return 1;
  if (queue.empty   () == false) return 1;
  if (queue.full    () == true)  return 1;

  // push our favorite message
  queue.push (hello);
  queue.push (world);

  // check the predicate again
  if (queue.empty   () == true)  return 1;
  if (queue.full    () == false) return 1;

  // pop and check
  String* sobj = dynamic_cast <String*> (queue.pop ());
  if (*sobj != *hello) return 1;
  sobj = dynamic_cast <String*> (queue.pop ());
  if (*sobj != *world) return 1;

  // reset and check
  queue.reset ();
  if (queue.empty   () == false) return 1;
  if (queue.full    () == false)  return 1;

  // push again
  queue.push (hello);
  queue.push (world);
  if (queue.getsize () != 64L) return 1;
  
  sobj = dynamic_cast <String*> (queue.get (0));
  if (*sobj != *hello) return 1;
  sobj = dynamic_cast <String*> (queue.get (1));
  if (*sobj != *world) return 1;

  // resize and check
  queue.resize (100);
  if (queue.getsize () != 100) return 1;
  if (queue.full () == true)   return 1;

  // ready to clean
  Object::dref (hello);
  Object::dref (world);

  // ok - everything is fine
  return 0;
}
