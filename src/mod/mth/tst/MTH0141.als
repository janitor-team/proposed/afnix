# ---------------------------------------------------------------------------
# - MTH0141.als                                                             -
# - afnix:mth module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 2019-2021 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   quaternion test unit
# @author amaury darsch

# get the service
interp:library "afnix-mth"

# create a quaternion object
trans q (afnix:mth:Quaternion)
assert true (afnix:mth:quaternion-p q)
assert "Quaternion" (q:repr)

# set by value and check
q:set 1.0 2.0 3.0 4.0
trans rval (q:get-real-part)
trans ival (q:get-imaginary-part)
assert 1.0 rval
assert 2.0 (ival:get-x)
assert 3.0 (ival:get-y)
assert 4.0 (ival:get-z)

# check x rotation
const a afnix:mth:HALF-PI
q:set-rotation-x a
assert 1.0  (q:module)
assert a    (q:argument) false
assert true (q:rotation-x-p)
trans  rv   (q:rotate (afnix:mth:Rvector3 0.0 1.0 0.0))
assert 0.0  (rv:get-x) false
assert 0.0  (rv:get-y) false
assert 1.0  (rv:get-z) false
trans  m    (q:to-rotation)
assert  1.0  (m:get 0 0) false
assert  0.0  (m:get 0 1) false
assert  0.0  (m:get 0 2) false
assert  0.0  (m:get 1 0) false
assert  0.0  (m:get 1 1) false
assert -1.0  (m:get 1 2) false
assert  0.0  (m:get 2 0) false
assert  1.0  (m:get 2 1) false
assert  0.0  (m:get 2 2) false
# check y rotation
q:set-rotation-y a
assert 1.0  (q:module)
assert a    (q:argument) false
assert true (q:rotation-y-p)
trans  rv   (q:rotate (afnix:mth:Rvector3 0.0 0.0 1.0))
assert 1.0  (rv:get-x) false
assert 0.0  (rv:get-y) false
assert 0.0  (rv:get-z) false
trans  m    (q:to-rotation)
assert  0.0  (m:get 0 0) false
assert  0.0  (m:get 0 1) false
assert  1.0  (m:get 0 2) false
assert  0.0  (m:get 1 0) false
assert  1.0  (m:get 1 1) false
assert  0.0  (m:get 1 2) false
assert -1.0  (m:get 2 0) false
assert  0.0  (m:get 2 1) false
assert  0.0  (m:get 2 2) false
# check z rotation
q:set-rotation-z a
assert 1.0  (q:module)
assert a    (q:argument) false
assert true (q:rotation-z-p)
trans  rv   (q:rotate (afnix:mth:Rvector3 1.0 0.0 0.0))
assert 0.0  (rv:get-x) false
assert 1.0  (rv:get-y) false
assert 0.0  (rv:get-z) false
trans  m    (q:to-rotation)
assert  0.0  (m:get 0 0) false
assert -1.0  (m:get 0 1) false
assert  0.0  (m:get 0 2) false
assert  1.0  (m:get 1 0) false
assert  0.0  (m:get 1 1) false
assert  0.0  (m:get 1 2) false
assert  0.0  (m:get 2 0) false
assert  0.0  (m:get 2 1) false
assert  1.0  (m:get 2 2) false
