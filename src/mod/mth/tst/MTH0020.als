# ---------------------------------------------------------------------------
# - MTH0020                                                             -
# - afnix:mth module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2021 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   utility test unit
# @author amaury darsch

# get the module
interp:library "afnix-mth"

trans r (afnix:mth:deg-to-rad 90)
assert afnix:mth:HALF-PI r false
trans d (afnix:mth:rad-to-deg r)
assert 90 d false

trans r  (afnix:mth:deg-to-rad 404 59 60)
assert r (/ afnix:mth:HALF-PI 2.0) false
trans d (afnix:mth:rad-to-deg r)
assert 45 d false
