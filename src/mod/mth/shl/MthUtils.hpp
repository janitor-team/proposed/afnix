// ---------------------------------------------------------------------------
// - MthUtils.hpp                                                            -
// - afnix:mth module - math utilities definition                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_MTHUTILS_HPP
#define  AFNIX_MTHUTILS_HPP

#ifndef  AFNIX_STRING_HPP
#include "String.hpp"
#endif
 
namespace afnix {

  /// This file containes a set of math utility functions. All functions
  /// are statically defined and operates mostly on reals.
  /// @author amaury darsch

  class MthUtils {
  public:
    /// convert degree to radian
    /// @param d the angle to convert
    static t_real degtorad (const t_real d);

    /// convert degree to radian
    /// @param d the angle in degree
    /// @param m the angle in minutes
    /// @param s the angle in seconds
    static t_real degtorad (const t_real d, const t_real m, const t_real s);

    /// convert radian to degree
    /// @param d the angle in radian
    static t_real radtodeg (const t_real r);
  };
}

#endif
