// ---------------------------------------------------------------------------
// - MthCalls.cpp                                                            -
// - afnix:mth module - math specific calls implementation                   -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cons.hpp"
#include "Real.hpp"
#include "Infix.hpp"
#include "Vector.hpp"
#include "MthUtils.hpp"
#include "MthCalls.hpp"
#include "Exception.hpp"
#include "transient.tcc"

namespace afnix {

  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // convert an infix string to a rvi

  Object* mth_torvi (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = (argv.valid () == false) ? 0L : argv->length ();
    // check for valid arguments
    if (argc == 1) {
      String s = argv->getstring (0);
	return Infix::torvi (s);
      }
    throw Exception ("argument-error", "too many arguments with to-rvi");
  }

  // convert an object to an infix string

  Object* mth_toinfix (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = (argv.valid () == false) ? 0L : argv->length ();
    // check for valid arguments
    if (argc != 1) {
      throw Exception ("argument-error", "too many arguments with to-infix");
    }
    Object* obj = argv->get (0);
    // check for a rvi
    Rvi* rvi = dynamic_cast <Rvi*> (obj);
    if (rvi != nullptr) {
      String* result = new String (Infix::tostring (*rvi));
      return result;
    }
    throw Exception ("type-error", "invalid object with to-infix",
		     Object::repr (obj));
  }

  // convert degree to radian

  Object* mth_degtorad (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = (argv.valid () == false) ? 0L : argv->length ();
    // check for 1 argument
    if (argc == 1) {
      t_real d = argv->getrint (0);
      t_real r = MthUtils::degtorad (d);
      return new Real (r);
    }
    if (argc == 3) {
      t_real d = argv->getrint (0);
      t_real m = argv->getrint (1);
      t_real s = argv->getrint (2);
      t_real r = MthUtils::degtorad (d, m, s);
      return new Real (r);
    }
    throw Exception ("arguement-error", "too many arguments with deg-to-rad");
  }

  // convert radian to degree

  Object* mth_radtodeg (Evaluable* zobj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    t_transient<Vector> argv = Vector::eval (zobj, nset, args);
    long argc = (argv.valid () == false) ? 0L : argv->length ();
    // check for 1 argument
    if (argc == 1) {
      t_real r = argv->getreal (0);
      t_real d = MthUtils::radtodeg (r);
      return new Real (d);
    }
    throw Exception ("arguement-error", "too many arguments with rad-to-deg");
  }
}
