// ---------------------------------------------------------------------------
// - MthUtils.cpp                                                            -
// - afnix:txt module - math utilities implementation                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2021 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Math.hpp"
#include "MthUtils.hpp"
 
namespace afnix {

  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // convert an angle in radian

  t_real MthUtils::degtorad (const t_real d) {
    // normalize the angle
    t_real a = d;
    while (a >= 360.0) a -= 360.0;
    while (a <  0.0)   a += 360.0;
    // convert the angle in radians
    return (Math::CV_TP / 360.0) * a;
  }

  // convert an angle in radian

  t_real MthUtils::degtorad (const t_real d, const t_real m, const t_real s) {
    // convert the angle
    t_real a = d + m / 60.0 + s / 3600.0;
    // convert in radians
    return degtorad (a);
  }

  // convert an angle in degree

  t_real MthUtils::radtodeg (const t_real r) {
    // normalize the angle
    t_real a = r;
    while (a >= Math::CV_TP) a -= Math::CV_TP;
    while (a <  0.0) a += Math::CV_TP;
    // convert in degree
    return (360.0 / Math::CV_TP) * a;
  }
}
