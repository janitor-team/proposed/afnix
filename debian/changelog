afnix (3.5.0-3) unstable; urgency=medium

  * Make the build reproducible for version info. (Closes: #998104)
    Add d/patches/0014-Reproducible-build.patch. Thanks to Chris Lamb
    <lamby@debian.org> to this patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Mon, 22 Nov 2021 14:24:58 +0900

afnix (3.5.0-2) unstable; urgency=medium

  * Drop build date from man pages for reproducible builds.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 14 Sep 2021 23:45:24 +0900

afnix (3.5.0-1) unstable; urgency=medium

  * New upstream release.
  * Drop unnecessary patches.
    - d/patches/0004-Fix-manpage-error.patch.
  * Fix FTBFS with gcc-11 (Closes: #983965)
  * Update d/copyright to DEP5.
  * Update d/control:
    - Add Rules-Requires-Root: no.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Mon, 30 Aug 2021 08:32:07 +0900

afnix (3.4.0-2) unstable; urgency=medium

  * Update d/patches/0008-sec_test_ctl.patch.
    Drop SEC0003.als for armhf.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sun, 31 Jan 2021 09:20:02 +0900

afnix (3.4.0-1) unstable; urgency=medium

  * New upstream release.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sun, 24 Jan 2021 14:59:07 +0900

afnix (3.3.0-4) unstable; urgency=medium

  * Upload to unstable.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sun, 27 Dec 2020 20:27:06 +0900

afnix (3.3.0-3) experimental; urgency=medium

  * Fix build on s390x.
    Add d/patches/0011-Fix-missing-support-s390x.patch.
  * Fix build on ppc64.
    Update Add d/patches/0012-Add-support-ppc64.patch.
  * Disable SEC0001.als test on sparc64.
  * Disable NWG0013.als test on powerpc.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sat, 26 Dec 2020 21:45:16 +0900

afnix (3.3.0-2) experimental; urgency=medium

  * Update d/patches/0008-sec_test_ctl.patch.
    Disable SEC0001.als on sparc64.
  * Fix build on ppc64.
    Add d/patches/0011-Add-support-ppc64.patch.
  * Add test control function for nwg.
    Add d/patches/0012-Disable-nwg-test.patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sat, 26 Dec 2020 18:14:52 +0900

afnix (3.3.0-1) experimental; urgency=medium

  * New upstream release. (Closes: #956987)
  * Update d/control.
    * Update debhelper dependency to "Build-Depends: debhelper-compat (= 13)".
    * Bump Standards-Version to 4.5.1 (no change).
  * Update d/patches.
    * Remove unnecessary patches.
    * Rebase upstream.
    * Update patches for gcc.
  * Drop a function for installing man files.
  * Add d/gbp.conf.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sat, 26 Dec 2020 09:34:50 +0900

afnix (2.9.2-2) unstable; urgency=medium

  * Fix build with gcc-9 (Closes: #925628)
    - Add d/patches/use-dpkg-buildflags-for-gcc9.patch.
    - Add d/patches/Add-support-gcc9.patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Wed, 09 Oct 2019 08:28:05 +0900

afnix (2.9.2-1) unstable; urgency=medium

  * New upstream release.
  * Remove unnecessary patches. These patches was applied to upstream.
    - d/patches/Add-support-RISCV.patch
    - d/patches/Fix-Hexadecimal-number-at-MT_MA.patch
    - d/patches/Remove-duplicate-getquad.patch
    - d/patches/resolve-comparison-signed-unsigned.patch

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Mon, 21 Jan 2019 11:35:16 +0900

afnix (2.9.0-3) unstable; urgency=medium

  * Add support RISCV.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Wed, 16 Jan 2019 08:05:09 +0900

afnix (2.9.0-2) unstable; urgency=medium

  * Fix build on 32bit architecture.
    Add d/patches/Fix-Hexadecimal-number-at-MT_MA.patch and
    d/patches/Remove-duplicate-getquad.patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Fri, 11 Jan 2019 22:17:16 +0900

afnix (2.9.0-1) unstable; urgency=medium

  * New upstream release.
  * Remove d/patches/add-support-gcc8.patch.
    Applied to upstream.
  * Remove d/patches/Fix-build-with-Werror-restrict-of-gcc-8.patch.
    Other implementations made this patch unnecessary.
  * Remove d/source/options.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Fri, 11 Jan 2019 08:48:02 +0900

afnix (2.8.1-2) unstable; urgency=medium

  * Update Vcs-Browser and Vcs-Git to salsa.
  * Update debhelper to 11.
  * Bump Standards-Version to 4.1.4.
  * Support gcc8. (Closes: #897700)
    - Add patches/use-dpkg-buildflags-for-gcc8.patch
    - Add patches/add-support-gcc8.patch
    - Add patches/Fix-build-with-Werror-restrict-of-gcc-8.patch

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sat, 09 Jun 2018 06:10:26 +0900

afnix (2.8.1-1) unstable; urgency=medium

  * New upstream release.
  * Remove patches/support_arm64.patch and patches/Add-support-gcc-7.patch.
    Applied to upstream.
  * Remove patches/i486-arch.patch. We already do not support gcc4.
  * Update Vcs-Browser and Vcs-Git.
  * Standards-Version to 4.1.1.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Mon, 20 Nov 2017 13:25:15 +0900

afnix (2.8.0-3) unstable; urgency=medium

  * Upload to unstable.
  * Cleanup patches.
    - Remove patches/remove_SYS0008.als.patch.
    - Add patches/sys_test_ctl.patch.
    - Remove patches/Disable-NET0001.als.patch.
    - Add patches/net_test_ctl.patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Thu, 28 Sep 2017 05:20:50 +0900

afnix (2.8.0-2) experimental; urgency=medium

  * Disable NET0003.als on armel, mips, s390x, hurd-i386, m68k, ppc64 and sh4.
    - Add patches/sec_test_ctl.patch.
    - Remove patches/Disable-SEC0004.als.patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Wed, 27 Sep 2017 09:14:22 +0900

afnix (2.8.0-1) unstable; urgency=medium

  * New upstream release.
  * Add Add-support-gcc-7.patch and use-dpkg-buildflags-for-gcc7.patch.
    (Closes: 853302)
  * Update debian/control.
    - Update Standards-Version to 4.1.0.
    - Update debhelper 10.
  * Add patches/Disable-SEC0004.als.patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Wed, 27 Sep 2017 06:27:40 +0900

afnix (2.6.3-1) unstable; urgency=medium

  * Update to 2.6.3.
  * Update patches/use-dpkg-buildflags-gcc6.patch.
    Basic code was applied to upstream. But not support overriding config yet.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Thu, 25 Aug 2016 04:22:27 +0900

afnix (2.6.2-3) unstable; urgency=medium

  * Fix build on arm64. (Closes: #814040)
    Add patches/support_arm64.patch.
    Thanks to Edmund Grimley Evans <edmund.grimley.evans@gmail.com>.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Mon, 22 Aug 2016 23:36:21 +0900

afnix (2.6.2-2) unstable; urgency=medium

  * Fix build on some architecture. (Closes: #815436)
    Add patches/resolve-comparison-signed-unsigned.patch.
  * Fix build with gcc6.
    Update patches/use-dpkg-buildflags-gcc6.patch.
  * Update Standards-Version to 3.9.8.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Fri, 05 Aug 2016 05:19:10 +0900

afnix (2.6.2-1) unstable; urgency=medium

  * Update to 2.6.2.
  * Remove patches. applied in upstream.
    debian/patches/mips64-support.patch
    debian/patches/ppc64el-support.patch
    debian/patches/sh4_support.patch

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sun, 07 Feb 2016 00:56:29 +0900

afnix (2.6.0-1) unstable; urgency=medium

  * Update to 2.6.0.
  * Add support gcc 6. (Closes: #812018)
    Add patches/use-dpkg-buildflags-gcc6.patch.
  * Remove libncurses5 from B-D. (Closes: #807092)
  * Update patches/Disable-NET0001.als.patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Thu, 28 Jan 2016 17:22:49 +0900

afnix (2.5.1-1) unstable; urgency=medium

  * New upstream release. (Closes: #789968)
  * Update debian/control.
    - Update Standards-Version to 3.9.6.
  * Add support mips64(el) and ppc64el. (Closes: #741508, #748146)
  * Add patches/support-gcc-5.x.patch. (Closes: #777767)
    - Fix build with gcc-5.x.
  * Add patches/Disable-NET0001.als.patch.
    - Disable test of NET0001.als.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sat, 11 Jul 2015 02:00:35 +0900

afnix (2.4.0-1) UNRELEASED; urgency=medium

  * New upstream release.
  * Update debian/control.
    - Update Standards-Version to 3.9.5.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 26 Aug 2014 09:44:41 +0900

afnix (2.3.1-1) UNRELEASED; urgency=low

  * New upstream release.
  * Update debian/control.
    - Add Vcs-Git and Vcs-Browser field.
    - Update Standards-Version to 3.9.4.
  * Update debian/rules.
    - Fix the function to change the name of manpage.
    - Remove install stuff for emacs-lisp.
  * Update patches/use-dpkg-buildflags.patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sat, 19 Oct 2013 15:20:06 +0900

afnix (2.2.0-2) unstable; urgency=low

  * Update debian/control.
    - Update Standards-Version to 3.9.3.
  * Fix build hardening flags missing. (Closes: #665381)
    - Add patches/use-dpkg-buildflags.patch and patches/verbose-build.patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 27 Mar 2012 08:02:44 +0900

afnix (2.2.0-1) unstable; urgency=low

  * New upstream release.
  * Update debian/rules.
    - Added build-arch and build-indep targets.
    - Use buildflags.mk from dpkg to set default compiler flags.
  * Update debian/control.
    Build-Depends on dpkg-dev (>= 1.16.1~) for buildflags.mk.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 10 Jan 2012 00:08:23 +0900

afnix (2.1.1-3) unstable; urgency=low

  * New maintainer. (Closes: #475377)
  * Update Standards-Version to 3.9.2.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Mon, 14 Nov 2011 19:16:09 +0900

afnix (2.1.1-2) unstable; urgency=low

  * QA upload.
  * Remove initialization code of sa_restorer.
    Update patches/fix_618706.patch. (Closes: #618706)

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 06 Sep 2011 13:04:13 +0900

afnix (2.1.1-1) unstable; urgency=low

  * QA upload.
  * New upstream release. (Closes: #618978)
  * Update debian/patches.
    - Add fix_618706.patch. (Closes: #618706)
    - Update ManPageErrors.patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Mon, 05 Sep 2011 15:22:48 +0900

afnix (2.0.0-1) unstable; urgency=low

  * QA upload.
  * New upstream release
  * Debian source format is 3.0 (quilt)
  * Fix debhelper-but-no-misc-depends
  * Fix ancient-standards-version
  * Fix package-contains-linda-override
  * debhelper compatibility is 7
  * Fix dh-clean-k-is-deprecated

 -- Anibal Monsalve Salazar <anibal@debian.org>  Wed, 16 Mar 2011 21:31:18 +1100

afnix (1.5.2-3.4) unstable; urgency=low

  * Non-maintainer upload.
  * Add support Renesas SH(sh4) (Closes: #555847).
    - patches/sh4_support.dpatch
  * Update debian/watch file (Closes: #551416).

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 25 Jan 2011 13:16:08 +0900

afnix (1.5.2-3.3) unstable; urgency=low

  * Non-maintainer upload.
  * Reverted patch to use g++-4.1 on arm (old architecture), as arm is no
    longer in the repository. Patch by Jonathan Niehof. (Closes: #533804)

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 04 Nov 2009 14:57:54 +0100

afnix (1.5.2-3.2) unstable; urgency=low

  * Non-maintainer upload.
  * Applied a patch by Martin Guy to use g++-4.1 on arm, as
    g++-4.2 and g++-4.3 both segfault.  (Closes: #463277)

 -- Philipp Kern <pkern@debian.org>  Mon, 11 Aug 2008 00:45:32 -0300

afnix (1.5.2-3.1) unstable; urgency=low

  * Non-maintainer upload.

  [ Maximiliano Curia ]
  * Added gcc-4.3_support.dpatch to fix FTBFS with GCC 4.3 (Closes: #461964)

 -- Peter Eisentraut <petere@debian.org>  Wed, 07 May 2008 13:45:20 +0200

afnix (1.5.2-3) unstable; urgency=low

  * afnix-doc: should be Architecture:all (Closes: #451602)
  * Fixed FTBFS: dpkg-shlibdeps: failure: couldn't find library libafnix-
    eng.so.1.5 needed by debian/afnix/usr/lib/afnix/libafnix-
    net.so.1.5.2. (Closes: #453794)
  * Added Homepage field to control.

 -- Paul Cager <paul-debian@home.paulcager.org>  Thu, 22 Nov 2007 00:05:22 +0000

afnix (1.5.2-2) unstable; urgency=low

  * Closes: #432888 - FTBFS on some arches
      'error: unrecognized command line option "-march=i486"'

 -- Paul Cager <paul-debian@home.paulcager.org>  Fri, 13 Jul 2007 13:13:04 +0100

afnix (1.5.2-1) unstable; urgency=low

  * New upstream release
     - Closes: #413847 - FTBFS on GNU/kFreeBSD.
     - Closes: #424089 - FTBFS if built twice in a row.

     This release also provides a xml processor in the form
     of a new service module called 'xml processing environment'
     or xpe.

  * Patched src/lib/std/shl/ucd/cucd.hpp
     Closes: #424547 - FTBFS with GCC 4.2: deprecated conversion from string
                       constant.

 -- Paul Cager <paul-debian@home.paulcager.org>  Wed, 20 Jun 2007 09:39:05 +0100

afnix (1.4.2-2) unstable; urgency=low

  * Fix file conflicts between afnix and aleph (Closes: #412952)

 -- Paul Cager <paul-debian@home.paulcager.org>  Thu,  1 Mar 2007 10:12:36 +0000

afnix (1.4.2-1) unstable; urgency=low

  * Initial Release (Closes: #379564).

 -- Paul Cager <paul-debian@home.paulcager.org>  Fri,  1 Dec 2006 23:09:54 +0000
